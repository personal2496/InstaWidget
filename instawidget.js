// Variables used by Scriptable.
// These must be at the very top of the file. Do not edit.
// icon-color: pink; icon-glyph: magic;

//@ts-check

let config = importModule('./Qiao/config');
let APP_ID = config.APP_ID;
let REDIRECT_URI = config.REDIRECT_URI;

let getAuthUrl = () => {
    return `https://api.instagram.com/oauth/authorize?client_id=${APP_ID}&redirect_uri=${REDIRECT_URI}&scope=user_profile,user_media&response_type=code`
}
/**
 * Create the widget
 * @param {{widgetParameter: string, debug: string}} config widget configuration
 */
 async function createWidget(config) {

  /*
  const log = config.debug ? console.log.bind(console) : function () {};
  log(JSON.stringify(config, null, 2))

  let message = 'Hello World!'
  let param = config.widgetParameter
  if (param != null && param.length > 0) {
      message = param
  }
  */
  // @ts-ignore
  let req = new Request(getAuthUrl());
  let test = await req.loadString();
  log(test);
  const widget = new ListWidget();

  widget.backgroundColor = new Color("#000000");

  // Add widget heading
  let heading = widget.addText("Test2");
  heading.centerAlignText();
  heading.font = Font.lightSystemFont(25);
  heading.textColor = new Color("#ffffff");

  return widget
}

module.exports = {
  createWidget
}


